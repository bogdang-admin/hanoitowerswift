//
//  hanoi.swift
//  hanoiTower
//
//  Created by Bogdan Goncharenko on 2016-03-09.
//  Copyright © 2016 SCS. All rights reserved.
//

//
//  main.swift
//  hanoiTower
//
//  Created by Bogdan Goncharenko on 2016-02-05.
//  Copyright © 2016 SCS. All rights reserved.
//

import Foundation

//: Playground - noun: a place where people can play

import Cocoa

struct Stack<Element> {
    var items = [Element]()
    mutating func push(_ item: Element) {
        items.append(item)
    }
    mutating func pop() -> Element {
        return items.removeLast()
    }
    
    func getTop()-> Element {
        return items[items.count - 1]
    }
    
    func getCount()->Int{
        return items.count;
    }
}

enum STACK_ENUM : Int{
    case left = 0, center, right,stack_COUNT
    
    func description()->String{
        switch self{
        case .left:
            return "Left";
        case .right:
            return "Right";
        case .center:
            return "Center";
        default:
            return "Unknown";
        }
        
    }
    
}

class field{
    var stacks = [Stack<Int>()]
    init(){
        
        // create the stacks
        // will populate it in the solve problem method
        for _ in 0...(STACK_ENUM.stack_COUNT.rawValue - 1){
            stacks.append(Stack<Int>())
        }
    }
    
    func findInversion(_ from:STACK_ENUM,to:STACK_ENUM)->STACK_ENUM{
        
        // TODO: find more elegant solution for this method
        
        if(from == STACK_ENUM.left && to == STACK_ENUM.right){
            return STACK_ENUM.center;
        }
        if(from == STACK_ENUM.right && to == STACK_ENUM.left){
            return STACK_ENUM.center;
        }
        
        if(from == STACK_ENUM.center && to == STACK_ENUM.right){
            return STACK_ENUM.left;
        }
        if(from == STACK_ENUM.right && to == STACK_ENUM.center){
            return STACK_ENUM.left;
        }
        
        if(from == STACK_ENUM.left && to == STACK_ENUM.center){
            return STACK_ENUM.right;
        }
        
        if(from == STACK_ENUM.center && to == STACK_ENUM.left){
            return STACK_ENUM.right;
        }
        
        
        return STACK_ENUM.right;
    }
    
    
    func moveElement(_ from:STACK_ENUM, to:STACK_ENUM){
        print("Moving element \(from.description()) to \(to.description())");
        
        if(stacks[to.rawValue].getCount() > 0 && stacks[from.rawValue].getCount() > 0){
            let valTo = stacks[to.rawValue].getTop();
            let valFrom = stacks[from.rawValue].getTop();
            assert(valTo > valFrom);
        }
        
        stacks[to.rawValue].push(stacks[from.rawValue].pop())
        
    }
    
    func isProblemSolved(_ numberTiles:Int)->Bool{
        if(stacks[STACK_ENUM.right.rawValue].items.count == numberTiles){
            return true;
        }
        
        return false;
    }
    
    func SolveProblem(_ numberTiles:Int){
        
        // populate left stack with the required number of items
        for elemIndex in 1...numberTiles {
            stacks[STACK_ENUM.left.rawValue].push(numberTiles - elemIndex + 1);
        }
        
        moveTask(STACK_ENUM.left, to: STACK_ENUM.right, numberTiles: numberTiles);
        assert(isProblemSolved(numberTiles) == true);
    }
    
    func moveTask(_ from:STACK_ENUM,to:STACK_ENUM,numberTiles:Int){
        
        if(numberTiles == 1){
            moveElement(from, to: to);
            return;
        }
        
        // split the moving into the smaller tasks
        let newDestination = findInversion(from,to:to);
        moveTask(from,to:newDestination,numberTiles: numberTiles - 1);
        moveTask(from,to:to,numberTiles: 1);
        moveTask(newDestination,to:to,numberTiles: numberTiles - 1);
    }
    
}


//field_var.SolveProblem(16);





